
// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
import {useState} from 'react';
// [S50 ACTIVITY END]


// [S50 ACTIVITY]
export default function CourseCard({course}) {

    // Deconstruct the course properties into their own variables
    const {name, description, price} = course;

    /*
    SYNTAX:
        const [getter, setter] = useState(initialGetterValue);
    */

    //discussion code

    // const [count, setCount] = useState(0);

    // function enroll(){
    //     setCount(count + 1)
    // }


// S51 Activity code 

const [count, setCount] = useState(0);
const [slots, setSlots] = useState(30);

  function enroll() {
    if (slots > 0) {
      setCount(count + 1);
      setSlots(slots - 1);
      if (count === 30) {
        alert("no more seats");
      }
    } else {
      alert("No more seats");
    }
  }



// S51 Activity code end

return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>PhP {price}</Card.Text>
                        <Card.Subtitle>Enrollees:</Card.Subtitle>
                        <Card.Text>{count} Enrollees</Card.Text>
                        <Button variant="primary" onClick={enroll}>Enroll</Button>

                    </Card.Body>
                </Card>
            </Col>
    </Row>        
    )
}
// [S50 ACTIVITY END]





